/*
 Estas funciones se usan para leer e interpretar los
 comandos del Shell
*/
#ifndef CommandReader_H_INCLUDED
#define CommandReader_H_INCLUDED

extern void ReadComand(char*); // Funcion que lee comandos UNIX
extern void OneCommand(char*); // Ejecuta comandos con solo una instruccion
extern void CommandWitArgs(char*,char*,int); // Ejecuta un comando con argumentos
extern void GetArgv(char[50],char[50],char*[50],int);
#endif
/* Implementacion de la Libreria */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include "CommandReader.h"
#include "Tuberias.h"
int valid = 0;
int npipes = 0;
int redireccion_salida = 0; // 1 si hay redirreccionamiento
int redireccion_entrada = 0; // 1 si hay redireccionamiento
int redireccion_error = 0; // 1 si hay redireccionamiento de error



// Auxiliar function
char *arguns[50];
// Crea el entorno para el execp
void GetArgv(char comando[50],char argumentos[50],char *argv[50],int argnum){
    //printf("Argumentos: %s\n",argumentos);
    argv[0] = malloc(strlen(comando) + 1);
    strcpy(argv[0],comando);
    int i= 0;
    int args = 1;
    char c = argumentos[i];
    char aux[50] = "";
    int j = 0;
    /*for(j=1; j <= argnum;j++){
        argv[j] = "";
        printf("argv[%s] \n",argv[j]);
    }
    j=0;
    */
    do{
      c = argumentos[i];  
      i++;
      if(c == '$' || c == '\n'){ // Guarda el ultimo argumento
              argv[args] = malloc(strlen(aux) + 1);
              strcpy(argv[args],aux);
              memset(aux,0,sizeof(aux));
              break;
      }else{
          if( c == ' '){ // Guarda el argumento en argv y limpia la variable auxiliar
              argv[args] = malloc(strlen(aux) + 1);
              strcpy(argv[args],aux);
              memset(aux,0,sizeof(aux));
              args++;
          }else{ // Guarda el argumento en una variable auxiliar
              char caux[2] = {c,'\0'};
              strcat(aux,caux);
          }
      }
    }while(c != '$');
    argv[args+1] = 0; //  Ultimo apuntador a 0
    /*for(i=0; i <= args;i++){
        printf("l: %s\n",argv[i]);
    }*/
}
void CommandWithArgs(char comando[50], char arglist[50],int argnum){ // Implementacion que ejecuta un comando con 
 // sus argumentos
  int process;
  if(fork() == 0){
      char path[100];
      strcpy(path,"/bin/");
      strcat(path,comando); // Construye el path del comando a ejecutar
      char *argv[50];// Arreglo de argumentos
      printf("Proceso %i => Ejecutando %s\n",getpid(),comando);
      GetArgv(comando,arglist,argv,argnum); // Construye el entorno argv
      valid = execvp(comando,argv); // Si hay algun error lo muestra
      if(valid == -1){
          printf("Error Comando o Argumentos Invalidos 😢 😱\n");
      }
  }else{
      wait(&process); // El Padre espera que el hijo haga el exec y luego
      /*
     Verifica las banderas para una vez terminado el exec, regrese la salida, entrada o error
     estandar a los indentificadores orginales
     */
      if(redireccion_salida == 1){
          restaurarSalidaEstandar();
          redireccion_salida = 0;
      }
      if(redireccion_error == 1){
         restaurarErrorEstandar();
          redireccion_error = 0;
      }
      if(redireccion_entrada == 1){
          restaurarEntradaEstandar();
          redireccion_entrada = 0;
      }
      // EL shell vuelve a esperar el siguiente comando
  }

}
void OneCommand(char * comando){ // Implementacion que ejecuta comandos de
// Una sola instruccion
 int process;
 if(fork() == 0){
     printf(" Proceso %i => Ejecutando: %s \n",getpid(),comando);
     valid =  execlp(comando,comando,NULL,0);
     if(valid == -1){
         printf("Error Comando Invalido 😢 😱\n");
     }
     // El hijo ejecuta un exec
 }else{
     wait(&process); // EL shell espera a que el hijo termine el exec
     /*
     Verifica las banderas para una vez terminado el exec, regrese la salida, entrada o error
     estandar a los indentificadores orginales
     */
     if(redireccion_salida == 1){
          restaurarSalidaEstandar();
          redireccion_salida = 0;
      }
      if(redireccion_error == 1){
         restaurarErrorEstandar();
         redireccion_error = 0;
      }
      if(redireccion_entrada == 1){
          restaurarEntradaEstandar();
          redireccion_entrada = 0;
      }
 }
}
void ReadCommand(char* comando){
    npipes = 0;
    printf("Solicitando 😉: %s \n",comando);
    char c; // Variable que guarda un caracter
    int i = 0; // Contador Auxiliar
    int com = 0; // Si es uno ya se leyo el comando
    int args = 0; // Cantidad de argumentos
    int args2 = 0;
    int tuberias;
    char  command[50] = ""; // Comando;
    char  arr[100] = ""; // Arreglo de comandos
    char  *argv[100];
    char  archivosal[100] = "";
    char archivoerror[100] = "";
    char archivoentrada[100] = "";
    char *pipescomands[10][2];
    int espacio = 0;
    int coms = 0;
    do{
        c = comando[i];
        i++;
        if(com == 0 ){ // Obtiene el command
            if(espacio == 0 && npipes > 0){
                if( c == ' '){
                    espacio++;
                }
            }else{
                if( (c != '\n' && c != ' ' && c != '|') ){
                char caux[2] = {c,'\0'};
                strcat(command,caux); // Construye el comando
            }else{
               if(c == '\n'){
                    // Si no tiene argumentos sale del loop
               }else{
                   args++;
               } 
               com++; // Activa la bandera de comando
            }
            }
            
        }
        else{ // Obtiene el arreglo de argumentos
           if( c == '|'){
               //printf("--- %s %s ---\n",command,arr);
               if(strlen(arr) == 0){
                   arr[strlen(arr)] = '$';

               }
               else{
                   arr[strlen(arr)-1] = '$'; 
               }
               pipescomands[coms][0] = malloc(strlen(command)+ 1);
               pipescomands[coms][1] = malloc (strlen(arr) + 1 );
               strcpy(pipescomands[coms][0],command);
               strcpy(pipescomands[coms][1],arr);
               memset(command,0,sizeof(command));
               memset(arr,0,sizeof(arr));
               com = 0;
               espacio = 0;
               coms++;
               npipes++; //  Lleva el control de cuantas pipes se deben crear

           }else{
             if( c == '>'){
               redireccion_salida = 1; // Activa la bandera de redireccion stdin
           }
           if( c == '%'){
               redireccion_error = 1; // Activa la bandera de redireccion stderr
           }
           if( c == '<'){
               redireccion_entrada = 1; // Activa la bandera de redireccion stdout
           }
           else{
               if( c == ' '){
               args++; // Cada espacio representa un nuevo argumento
              }
           if( c == '\n'){
                 break; // Sale del loop
           }else{
               char caux[2] = {c,'\0'};
               if(redireccion_salida == 0 && redireccion_error == 0 ){
                   strcat(arr,caux); // Lee argumentos
                   args2++;
               }else{
                   if( c == '>' || c == '%' || c == '<' || c == ' ' || c == '|'){
                     // ignora estos caracteres <, >, %
                   }
                   else{
                    strcat(archivosal,caux); // Lee el archivo de salida a redireccionar
                    strcat(archivoerror,caux);
                    strcat(archivoentrada,caux);
                   }
               }
             }
           }
          
           }
        }
           
          //printf("%s %s",argv[0],argv[1]);
        
    }while(c != '\n');
    //printf("Comamand: %s \n",command);
   if(redireccion_entrada == 1){
       redirEntrada(archivosal);
       arr[args2-1] = '$'; // Redirecciona la entrada a un archivo
   } 
   else{
       arr[args2]='$';
   }
   if(redireccion_salida == 1){
       redirSalida(archivosal); // Redirecciona la saliida a un archivo
       arr[args2-1] = '$';
   }else{
       arr[args2]='$';
   }
   if(redireccion_error == 1){
       redirError(archivoerror);
       arr[args2-1] = '$';
   }else{
       arr[args2]='$';
   }
   // $ representa fin del comando
    if( npipes > 0){ // Hay Manejo de Pipes
     if(strlen(arr) == 0){
                   arr[strlen(arr)] = '$';

               }
               else{
                //   arr[strlen(arr)] = '$'; 
     }
      pipescomands[coms][0] = malloc(strlen(command)+ 1);
      pipescomands[coms][1] = malloc (strlen(arr) + 1 );
      strcpy(pipescomands[coms][0],command);
      strcpy(pipescomands[coms][1],arr);
      if( fork() == 0){
         adminPipes(npipes,pipescomands);
      }else{
        wait(&tuberias);
        // EL Shell continua
      }

    }else{ // Se maneja la salida , entrada y error estandar
      if((args == 2 && redireccion_salida == 1) ||(args > 0 && redireccion_error == 1) || args == 0){
       OneCommand(command); // Para comandos sin argumentos

    }else{
       CommandWithArgs(command,arr,args); // Para comandos que aceptan argumentos
       }
    }
   
}
/* Funciones auxiares para pipes*/
void adminPipes(int numero_pipes,char *lista[10][2]){
    printf("--------------------\n");
    int i;
    int tee = 0;
    int readbytes;
    int guardado = 0;
    for( i=0; i<numero_pipes; i++)
    {
        int pd[2];
        pipe(pd);
        if (!fork()) {
            dup2(pd[1], 1); // salida a pipe
            if(strlen(lista[i][1]) == 1){
                    char *argv[2];
                    argv[0] = lista[i][0];
                    argv[1] = 0;
                    execvp(lista[i][0],argv);

                }else{
                    char *argv[50];
                    int l =  strlen(lista[i][1]);
                    lista[i][1][l] = '$';
                    GetArgv(lista[i][0],lista[i][1],argv,3);
                    execvp(lista[i][0],argv);
                }
             perror("exec");
             abort();
        }

        // la entrada viene del pipe
        dup2(pd[0], 0);
        close(pd[1]);
    }
    if( lista[i][0][0] == '{'){
        char *salida;
        FILE * archivo = fopen(lista[i][1],"w");
        close(archivo);
        redirSalida(archivo);
        printf("No Mames");
        restaurarSalidaEstandar();
        

    }else{
        if(strlen(lista[i][1]) == 1){
                    char *argv[2];
                    argv[0] = lista[i][0];
                    argv[1] = NULL;
                    execvp(lista[i][0],argv);

                }else{
                    char *argv[50];
                    int l =  strlen(lista[i][1]);
                    lista[i][1][l] = '$';
                    GetArgv(lista[i][0],lista[i][1],argv,3);
                    execvp(lista[i][0],argv);
                }
    perror("exec");
    abort();

    }

    
  }
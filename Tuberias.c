// Implementacion de Tuberias.h
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <fcntl.h>
#include "Tuberias.h"
int guardado ;// preserva la salida estándar
int error_estandar;
int entrada_estandar;
void crear_archivo(char * archivo){ // Funcion que crea un archivo desde 0
    FILE *F;
    F = fopen(archivo,"w");
    fclose(F);
}
void restaurarSalidaEstandar(){ // Devuelve el control a la salida estandar
    close(1); // Cierra el fichero
    dup(guardado); // Devuelve el control a la salida estandar
    close(guardado); // Cierra el proceso
}
void redirSalida(char* archivo){ // Redirecciona la salida estandar hacia un arhivo
   guardado = dup(1); // Guarda la salida estandar
   crear_archivo(archivo);
   close (1); // Cierra la salida estandar
   open(archivo,O_WRONLY,0777); // Se realiza el direccionamiento
   printf("Redireccionando a: %s\n", archivo);
}
void restaurarErrorEstandar(){ // Devuelve el control a la salida estandar
    close(2); // Cierra el fichero
    dup(error_estandar); // Devuelve el control a la salida estandar
    close(error_estandar); // Cierra el proceso
}
void redirError(char * archivo){
   error_estandar = dup(2); // Guarda la salida estandar
   crear_archivo(archivo);
   close (2); // Cierra la salida estandar
   open(archivo,O_WRONLY,0777); // Se realiza el direccionamiento
   printf("Redireccionando a: %s\n", archivo);

}
void restaurarEntradaEstandar(){
    close(0); // Cierra el fichero
    dup(entrada_estandar); // Devuelve el control a la salida estandar
    close(entrada_estandar); 
}
void redirEntrada(char* archivo){
   entrada_estandar = dup(0); // Guarda la entrada estandar
   close (0); // Cierra la entrada estandar
   open(archivo,O_RDONLY,0777); // Se realiza el direccionamiento

}
void execPipes(int *pipes[],int pipen){
    if(pipen == 1){
        printf("Abriendo Ultima Pipe \n");

    }else{
        printf("Abriendo Pipe : %d\n",pipen);
    }
}

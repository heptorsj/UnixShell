/*
Proyecto: Construcción de un Shell de UNIX
por:
1.- Jacales Rojas Héctor Daniel
*/
/*-------------------------------------------------
  Librerias para el Proyecto:
   El Shell debera ser manejador atravez de hilos,
   procesos duros y señales.
  */
#include <stdio.h> // Para leer comandor desde Entrada Estandar
#include <stdlib.h> // Libreria de C
#include <signal.h> // Para el manejo de señales
#include <pthread.h> // Para el uso de hilos
#include <string.h> // Para el procesamiento de cadenas de texto
#include <unistd.h> // Usada para hilos
#include <time.h> // Para obtener datos de fechas
#include "CommandReader.h" // Libreria auxiliar para exec
// Mensajes de Bienvenida de la Shell
typedef char * string;
string shellname = "\t\t◢ ◤ MyUnix-Shell ◢ ◤ ";
string prompt ="# "; // Prompt Original
int salirval = 0; // 1 indica activado
/*
  Funciones para capturar señales
*/
void modPromptBySignal(int);
/* Para mantener la bitacora a travez de hilos*/

void *UpdBitacora(void * arg){
 FILE *bitacora;
 bitacora = fopen(".bitacora","a");
 char * command;
 command = (char *) arg;
 fprintf(bitacora,"%s",command);
 fclose(bitacora);
}

// Inicio del Shell
int main(){
  printf("%s \n\n", shellname  );
  pthread_t actualizaBitacora;
  pthread_t ejecComando;
  FILE *bitacora;
  bitacora = fopen(".bitacora","w");
  FILE *grabadora = fopen("grabadora","w");
  close(grabadora);
  // Guarda el valor de inicio de sesion en la bitacora
  time_t fecha;
  struct tm * timeinfo;
  time ( &fecha );
  timeinfo = localtime ( &fecha );
  fprintf ( bitacora,"Fecha De Inicio: %s",asctime (timeinfo) );
  fclose(bitacora);
  signal(29,modPromptBySignal); // Modifica el capturador de señales
  signal(30,modPromptBySignal); // Modifica el capturador de señales
  signal(31,modPromptBySignal); // Modifica el capturador de señales
  signal(15,modPromptBySignal); // Modifica el capturador de señales
  while(1){ // Bucle infinito para el Shell
   //Esperando el comando
   char command[100];
   printf("%s",prompt); //Imprime el promt
   fgets (command, 100, stdin); // Lee el comando
   pthread_create(&actualizaBitacora,NULL,UpdBitacora,(void*) command); // Hilo que guarda el historial
   pthread_join(actualizaBitacora,NULL); // Espera a que el hilo termine
   ReadCommand(&command);
  }

}
void modPromptBySignal(int signum){
  switch(signum){
    case 29:
     prompt = "=D ";
     printf("\n%s", prompt );
    break;
    case 30:
     prompt = "=p ";
     printf("\n%s", prompt );
    break;
    case 31:
     prompt = "O_o ";
     printf("\n%s",prompt );
    break;
    default:
     prompt = "I <3 BSD ";
     printf("\n%s",prompt );
    break;
  }
}
// gcc -o Tuberias.o -c Tuberias.c
// gcc -o CommandReader.o -c CommandReader.c
// gcc UnixShell.c -o Shell CommandReader.o -pthread Tuberias.o
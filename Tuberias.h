/* Libreria usada para pipes, tees y redireccionamiento de E,S Y Error*/
#ifndef CommandReader_H_INCLUDED
#define CommandReader_H_INCLUDED

extern void redirSalida(char* nombre);
extern void restaurarSalidaEstandar();
extern void redirError(char* nombre);
extern void restaurarErrorEstandar();
extern void redirEntrada(char* nombre);
extern void restaurarEntradaEstandar();
extern void crearPipes(int *pipes[],int pipe);
#endif